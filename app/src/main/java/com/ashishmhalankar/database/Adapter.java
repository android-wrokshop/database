package com.ashishmhalankar.database;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private List<ContactObjectBOx> list;
    private Context context;
    private LayoutInflater inflater;

    private List<ContactObjectBOx> ListFiltered;
    private ContactsAdapterListener listener;


    public Adapter(Context context, List<ContactObjectBOx> feedsList, ContactsAdapterListener listener) {

        this.context = context;
        this.list = feedsList;
        this.ListFiltered = list;
        this.listener = listener;


        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView = inflater.inflate(R.layout.adapter_contact, parent, false);

        return new MyViewHolder(rootView);


    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ContactObjectBOx list = this.list.get(position);
        holder.tvName.setText(list.Name);
        holder.tvNumber.setText(list.mobileNumber);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvName, tvNumber;


        public MyViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            tvNumber = itemView.findViewById(R.id.tvNumber);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(ListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }
    public interface ContactsAdapterListener {
        void onContactSelected(ContactObjectBOx contact);
    }

}