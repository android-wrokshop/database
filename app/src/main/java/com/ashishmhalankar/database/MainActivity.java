package com.ashishmhalankar.database;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.objectbox.Box;

public class MainActivity extends AppCompatActivity implements Adapter.ContactsAdapterListener {

    Box<ContactObjectBOx> contactObjectBOx;

    EditText edName, edNumber;
    Button btnAdd;
    Adapter adapter;
    long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contactObjectBOx = ObjectBox.get().boxFor(ContactObjectBOx.class);
        init();
        list();

    }

    private void init() {
        edName = findViewById(R.id.edName);
        edNumber = findViewById(R.id.edNumber);
        btnAdd = findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    if (btnAdd.getText().toString().equals("add")) {
                        addContact(edName.getText().toString(), edNumber.getText().toString());

                    } else if (btnAdd.getText().toString().equals("Update")) {
                        updateContact(edName.getText().toString(), edNumber.getText().toString(), id);

                    }
                    list();


                }
            }
        });
    }

    Boolean isValid() {
        if (edName.getText().toString().isEmpty()) {
            edName.setError("Enter Name");
            return false;

        }
        if (edNumber.getText().toString().isEmpty()) {
            edNumber.setError("Enter Number");
            return false;

        }
        return true;
    }

    private void addContact(String name, String mobileNumber) {
        ContactObjectBOx objectBOx = new ContactObjectBOx();
        objectBOx.Name = name;
        objectBOx.mobileNumber = mobileNumber;
        contactObjectBOx.put(objectBOx);


    }

    private void updateContact(String name, String mobileNumber, long id) {
        ContactObjectBOx objectBOx = contactObjectBOx.get(id);
        objectBOx.Name = name;
        objectBOx.mobileNumber = mobileNumber;
        contactObjectBOx.put(objectBOx);
        btnAdd.setText("add");

        list();


    }


    void list() {
        if (contactObjectBOx.getAll().size() >= 0) {
            RecyclerView RecyclerView = findViewById(R.id.RecyclerView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            RecyclerView.setLayoutManager(layoutManager);

            if (this != null) {
                adapter = new Adapter(this, contactObjectBOx.getAll(), this);
                RecyclerView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onContactSelected(final ContactObjectBOx contact) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose a Action");
        String[] animals = {"Update", "Delete"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        edNumber.setText(contact.mobileNumber);
                        edName.setText(contact.Name);
                        btnAdd.setText("Update");
                        id = contact.id;
                        break;
                    case 1: // cow
                        contactObjectBOx.remove(contact.id);
                        list();
                        break;

                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
